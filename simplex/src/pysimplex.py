import unittest
import numpy as np
from IPython import embed
import collections


class Tableau:
    rows: np.array
    basis: list
    def __init__(self, A: np.array, b : np.array, c : np.array, basis: list):
        twoDimB = np.expand_dims(b, axis=0)
        varRows = np.concatenate((A, twoDimB.T), axis=1)
        cRow = np.concatenate((c, np.array([[0]])), axis=None)
        cRow = np.expand_dims(cRow, axis=0)
        self.rows = np.concatenate((varRows, cRow), axis = 0)
        self.basis = basis
        pass
    def getPivotColumn(self):
        maxValue = 0
        maxIndex = -1
        for index, value in enumerate(self.rows[-1], 0):
            if (value > maxValue):
                maxValue = value
                maxIndex = index
        return maxIndex
    def getPivotRow(self, pivot: int):
        pivotColumn = self.rows[:,pivot]
        if (pivotColumn < 0).all():
            return -1
        quotients = []
        for index, value in enumerate(pivotColumn, 0):
            if (value <= 0):
                quotients.append(-1)
                continue
            quotient = self.rows[index][-1]/value
            quotients.append(quotient)
        quotients = np.array(quotients)
        minPositive = min(quotients[quotients > 0])
        return np.argwhere(quotients == minPositive)[0][0]

   
    def changeBasis(self, pivotRow:int, pivotColumn: int):
        self.rows[pivotRow] = self.rows[pivotRow]/self.rows[pivotRow][pivotColumn]
        for index, row in enumerate(self.rows, 0):
            if index == pivotRow:
                continue
            factor = row[pivotColumn]/self.rows[pivotRow][pivotColumn]
            self.rows[index] = np.subtract(self.rows[index], factor * self.rows[pivotRow])
        
        return 1
    
    def getResult(self):
        result = []
        for column in self.rows.T[:-1]:
            counter = collections.Counter(column)
            if ((counter.get(0) == len(column) - 1) and (counter.get(1) == 1)):
                index = np.where(column == 1)[0][0]
                result.append(self.rows[index][-1])
            else: 
                result.append(0)
        return np.array(result)

    def isOptimal(self):
        return (self.rows[-1] <= 0).all()


class SimplexAlgorithm:
    tableau: Tableau
    solution: np.array
    def __init__(self, A: np.array, b: np.array, c: np.array, start_basis: list):
        self.tableau = Tableau(A=A, b=b, c=c, basis=start_basis)
    def solve(self, maximize=True) -> bool:
        while not self.tableau.isOptimal():
            pivotColumn = self.tableau.getPivotColumn()
            pivotRow = self.tableau.getPivotRow(pivotColumn)
            if pivotRow == -1: # infinite
                return False
            self.tableau.changeBasis(pivotRow, pivotColumn)
        
        self.solution = self.tableau.getResult()
        return True

    def get_solution(self) -> np.array:
        return self.solution



class SimplexAlgorithmTest(unittest.TestCase):
    def test_1(self):
        A = np.array([[0, 1, 1, 0, 0],
        [1, 1, 0, 1, 0],
        [1, -1, 0, 0, 1]])
        b = np.array([5, 7, 3])
        c = np.array([1, 0, 0, 0, 0])
        x = np.array([5, 2, 3, 0, 0])
        sa = SimplexAlgorithm(A=A, b=b, c=c, start_basis=[2, 3, 4])
        success = sa.solve()
        self.assertTrue(success)
        self.assertTrue((sa.get_solution()==x).all())


unitTests = SimplexAlgorithmTest()
unitTests.test_1()
embed()