import numpy as np
from IPython import embed
from numpy.linalg import LinAlgError
import sys
import itertools

def handleInput(input):
    lines = input.split('\n')
    lineArrays = [x.split(' ') for x in lines]
    optArray = lineArrays[0]
    optType = optArray[0]
    costVector = optArray[1:]
    bVector = [x[-1] for x in lineArrays[1:]]
    matrixA = [x[:-2] for x in lineArrays[1:]]
    
    return optType, convertToNumpy(costVector), convertToNumpy(bVector), convertToNumpy(matrixA)

def convertToNumpy(input):
    return np.asarray(input).astype(int)

def computeBasicSolution(matrixA, bVector, indexArray):
    solutionVector = np.zeros((len(matrixA[0])))
    abMatrix = createAbMatrix(matrixA, indexArray)
    try:
        inverse = np.linalg.inv(abMatrix)
        solution = np.matmul(inverse,bVector)
    except LinAlgError:
        return -1
    counter = 0
    for index in indexArray:
        solutionVector[index] = solution[counter]
        counter += 1 
    return solutionVector

def createAbMatrix(matrixA, indexArray):
    abMatrix = np.zeros((len(matrixA), len(indexArray)))
    counter = 0
    for i in indexArray:
        for index, row in enumerate(matrixA, 0):
            abMatrix[index][counter] = row[i]
        counter += 1
        
    return abMatrix
def computeBasicSolutionValue(costVector, basicSolution):
    
    return np.matmul(costVector, basicSolution)

def isValidSolution(solution):
    return (solution >= 0).all()


def readInput(path):
    f = open(path, 'r')
    return f.read()


def getPermutationsIndexes(size, maxIndex):
    elements = range(0, maxIndex)
    return list(itertools.permutations(elements, size))

def isMoreOptimal(prev, new, optType):
    if prev == None:
        return True
    if optType == 'min':
        if new < prev:
            return True
        else: 
            return False
    if optType == 'max':
        if new > prev:
            return True
        else: 
            return False

def optimize(optType, costVector, bVector, matrixA):
    rank = len(matrixA)
    optValue = None
    permutations = getPermutationsIndexes(rank, len(matrixA[0]))
    for perm in permutations:
        solution = computeBasicSolution(matrixA, bVector, perm)
        if (isValidSolution(solution)):
            value = computeBasicSolutionValue(costVector, solution)
            if (isMoreOptimal(optValue, value, optType)):
                optValue = value

    return optValue

content = readInput(sys.argv[1])
optType, costVector, bVector, matrixA = handleInput(content)
opt = optimize(optType, costVector, bVector, matrixA)
print(opt)
# embed()